const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const { Expo } = require('expo-server-sdk');
const request =  require('request');
const axios =  require('axios');
const _ = require('underscore');

const config = require('./config.js');

var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://esefosseoutracor:esefosseoutracor2020@devstream.pt:27017/esefosseoutracor";

app.use(bodyParser.json());
app.listen(3000, () => {
 console.log("Server running on port 3000");
});

app.get("/users", (req, res, next) => {
    MongoClient.connect(mongoUrl, function(err, db) {
        if (err) throw err;
        var dbo = db.db("esefosseoutracor");
        dbo.collection("users").find({}, function (err, docs) {
            res.json(docs);
        });
    })
});


app.post("/users", (req, res, next) => {
    const data = req.body;
    if(data.token && data.id){
        MongoClient.connect(mongoUrl, function(err, db) { 
            if (err) throw err;
            var dbo = db.db("esefosseoutracor");
            dbo.collection("users").findOne({id: data.id}, function (err, user) {
                if(user) {
                    console.log("Already exists")
                    res.json(false);
                } else {
                    dbo.collection("users").insert({ id: data.id, token: data.token, notifications: {POSTS: true, EVENTS: true} }, function (err) {
                        console.log(`Insert`, { id: data.id, token: data.token, notifications: {POSTS: true, EVENTS: true} });
                        db.close();
                        res.json(true);
                    });
                }
            });
        });
    } else {
        db.close();
        res.json(false);
    }
});

app.post("/users/:userId", (req, res, next) => {
    const data = req.body;
    const userId = req.params.userId;
    MongoClient.connect(mongoUrl, function(err, db) { 
        if (err) throw err;
        var dbo = db.db("esefosseoutracor");
        dbo.collection("users").update({ id: userId }, { $set: { notifications: data } }, { multi: true }, function (err, numReplaced) {
            console.log("err", err);
            console.log("updated", { id: userId }, { $set: { notifications: data } });
            db.close();
            res.json(true);
        });
    });
});

app.get("/users/:userId", (req, res, next) => {
    const userId = req.params.userId;
    MongoClient.connect(mongoUrl, function(err, db) { 
        if (err) throw err;
        var dbo = db.db("esefosseoutracor");
        dbo.collection("users").findOne({ id: userId }, function (err, user) { 
            console.log("err", err);
            console.log("get", user);
            db.close();
            res.json(user);
        })
    });
});




const checkPosts = () => axios
.post(`${config.cmsURL}/auth/local`, {
    identifier: config.identifier,
    password: config.password
})
.then(async (response) => {
    const token = response.data.jwt;
   
    request.get(`${config.cmsURL}/posts`, {
        'auth': {
          'bearer': token
        }
      },
      function (error, response, body) {
        if (error) {
          return console.error('Request failed:', error);
        }
        try {
          body = JSON.parse(body);
          if(body.length > 0) {
              let hasNewPosts = false;
              body.forEach((post, idx, array) => {
                MongoClient.connect(mongoUrl, function(err, db) { 
                    if (err) throw err;
                    var dbo = db.db("esefosseoutracor");
                    dbo.collection("notifications").findOne({ id: post._id }, function (err, notification) { 
                        if(_.isEmpty(notification)) {
                            hasNewPosts = true;
                            dbo.collection("notifications").insert({ id: post._id }, function (err) {});
                        }
                        if (idx === array.length - 1){ 
                            db.close();
                            if(hasNewPosts) {
                                sendNotification("Foram adicionados novos destaques.", "POSTS");
                            }
                        }
                    })
                });
              });
          }
        } catch (e) {
          console.log(e);
        }
      });
})
.catch(error => {
    console.log('An error occurred:', error);
});

const checkEvents = () => axios
.post(`${config.cmsURL}/auth/local`, {
    identifier: config.identifier,
    password: config.password
})
.then(async (response) => {
    const token = response.data.jwt;
   
    request.get(`${config.cmsURL}/eventos`, {
        'auth': {
          'bearer': token
        }
      },
      function (error, response, body) {
        if (error) {
          return console.error('Request failed:', error);
        }
        try {
          body = JSON.parse(body);
          if(body.length > 0) {
              let hasNewPosts = false;
              body.forEach((post, idx, array) => {
                MongoClient.connect(mongoUrl, function(err, db) { 
                    if (err) throw err;
                    var dbo = db.db("esefosseoutracor");
                    dbo.collection("notifications").findOne({ id: post._id }, function (err, notification) { 
                        if(_.isEmpty(notification)) {
                            hasNewPosts = true;
                            dbo.collection("notifications").insert({ id: post._id }, function (err) {});
                        }
                        if (idx === array.length - 1){ 
                            if(hasNewPosts) {
                                db.close();
                                sendNotification("Foram adicionados novos eventos.", "EVENTS");
                            }
                        }
                    })
                });
              });
          }
        } catch (e) {
          console.log(e);
        }
      });
})
.catch(error => {
    console.log('An error occurred:', error);
});

  
setInterval(() => {
    checkPosts();
    checkEvents();
}, 60000);

const sendNotification = (notificationText, notificationType) => {
    console.log(`Sending notification -> ${notificationText}`);
    MongoClient.connect(mongoUrl, function(err, db) { 
        if (err) throw err;
        var dbo = db.db("esefosseoutracor");
        dbo.collection("users").find({}).toArray( function (err, users) {
            users = _.filter(users, function(user){ return user.notifications[notificationType]; });
            const somePushTokens = _.pluck(users,"token"); 
            db.close();
            // Create a new Expo SDK client
            let expo = new Expo();

            // Create the messages that you want to send to clents
            let messages = [];
            for (let pushToken of somePushTokens) {
            // Each push token looks like ExponentPushToken[xxxxxxxxxxxxxxxxxxxxxx]

            // Check that all your push tokens appear to be valid Expo push tokens
            if (!Expo.isExpoPushToken(pushToken)) {
                console.error(`Push token ${pushToken} is not a valid Expo push token`);
                continue;
            }

            // Construct a message (see https://docs.expo.io/versions/latest/guides/push-notifications.html)
            messages.push({
                to: pushToken,
                sound: 'default',
                body: notificationText
            })
            }

            // The Expo push notification service accepts batches of notifications so
            // that you don't need to send 1000 requests to send 1000 notifications. We
            // recommend you batch your notifications to reduce the number of requests
            // and to compress them (notifications with similar content will get
            // compressed).
            let chunks = expo.chunkPushNotifications(messages);
            let tickets = [];
            (async () => {
            // Send the chunks to the Expo push notification service. There are
            // different strategies you could use. A simple one is to send one chunk at a
            // time, which nicely spreads the load out over time:
            for (let chunk of chunks) {
                try {
                let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                console.log(ticketChunk);
                tickets.push(...ticketChunk);
                // NOTE: If a ticket contains an error code in ticket.details.error, you
                // must handle it appropriately. The error codes are listed in the Expo
                // documentation:
                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                } catch (error) {
                console.error(error);
                }
            }
            })();


            // Later, after the Expo push notification service has delivered the
            // notifications to Apple or Google (usually quickly, but allow the the service
            // up to 30 minutes when under load), a "receipt" for each notification is
            // created. The receipts will be available for at least a day; stale receipts
            // are deleted.
            //
            // The ID of each receipt is sent back in the response "ticket" for each
            // notification. In summary, sending a notification produces a ticket, which
            // contains a receipt ID you later use to get the receipt.
            //
            // The receipts may contain error codes to which you must respond. In
            // particular, Apple or Google may block apps that continue to send
            // notifications to devices that have blocked notifications or have uninstalled
            // your app. Expo does not control this policy and sends back the feedback from
            // Apple and Google so you can handle it appropriately.
            let receiptIds = [];
            for (let ticket of tickets) {
            // NOTE: Not all tickets have IDs; for example, tickets for notifications
            // that could not be enqueued will have error information and no receipt ID.
            if (ticket.id) {
                receiptIds.push(ticket.id);
            }
            }

            let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);
            (async () => {
            // Like sending notifications, there are different strategies you could use
            // to retrieve batches of receipts from the Expo service.
            for (let chunk of receiptIdChunks) {
                try {
                let receipts = await expo.getPushNotificationReceiptsAsync(chunk);
                console.log(receipts);

                // The receipts specify whether Apple or Google successfully received the
                // notification and information about an error, if one occurred.
                for (let receipt of receipts) {
                    if (receipt.status === 'ok') {
                    continue;
                    } else if (receipt.status === 'error') {
                    console.error(`There was an error sending a notification: ${receipt.message}`);
                    if (receipt.details && receipt.details.error) {
                        // The error codes are listed in the Expo documentation:
                        // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                        // You must handle the errors appropriately.
                        console.error(`The error code is ${receipt.details.error}`);
                    }
                    }
                }
                } catch (error) {
                console.error(error);
                }
            }
            })();
        });
    });
}